macOS
------

- [launchDaemon](https://stuffjasondoes.com/2019/01/02/macos-mojave-startup-daemons-and-agents-beyond-the-gui/)
- [Edit plist](https://stackoverflow.com/questions/13740337/modifying-a-plist-from-command-line-on-mac-using-defaults)
- [Wake up](https://support.apple.com/en-us/HT204760)
- [Reset NVRAM](https://support.apple.com/en-us/HT204063)