Network layer 3
------

- [Layer 3 switch example](https://documentation.meraki.com/MS/Layer_3_Switching/Layer_3_Switch_Example)    
- [Laver 3 overview](https://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst4500/12-2/25ew/configuration/guide/conf/l3_int.html#wpxref32074)     
- [Layer 3 VS layer 2](https://documentation.meraki.com/MS/Layer_3_Switching/Layer_3_vs_Layer_2_Switching)    
- [VLAN understanding](https://blog.monstermuffin.org/understanding-and-configuring-vlans/)    
- [VLAN tagging](https://documentation.meraki.com/zGeneral_Administration/Tools_and_Troubleshooting/Fundamentals_of_802.1Q_VLAN_Tagging)    
- [VLAN cisco configuration](https://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst4500/12-2/25ew/configuration/guide/conf/vlans.html)
- [BSD Routing](https://docs.freebsd.org/doc/7.1-RELEASE/usr/share/doc/handbook/network-routing.html)
