Commands
------

List jobs loaded:
```
sudo launchctl list
```

Stop service:
```
sudo launchctl unload ${PLIST_FILE}
```

Start service:
```
sudo launchctl load ${PLIST_FILE}
```