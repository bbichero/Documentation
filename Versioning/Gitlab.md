Gitlab
------

Push over https when 2FA is enable:
- You need to create an access token with API right
- Change file `.git/config` in your projet and change the remote 'origin' url with :
```
https://oauth2:<mytoken>@git.example.com/myuser/myrepo.git
```
