Errors
------

```
“RTNETLINK answers: File exists” when running ifup
```

You need to flush network device before `ifup` and `ifdown`   
```
sudo ip addr flush dev ${NET_INT}
```

When creating a virsh network, at network start you got:
```
error: Failed to start network NETWORK_NAME
error: The name org.fedoraproject.FirewallD1 was not provided by any .service files
```

You just need to restart libvirt (no explaination found)
```
sudo systemctl restart libvirtd
sudo virsh net-start NETWORK_NAME
```

### UFW and Docker
Docker rewrite all ufw rules by default, to fix issue you need to add custom rule in `/etc/ufw/after.rules`

This configuration is for a single VM, WITH NO PRIVATE NETWORK
```
# BEGIN UFW AND DOCKER
*filter
:ufw-user-forward - [0:0]
:DOCKER-USER - [0:0]
-A DOCKER-USER -j RETURN -s 172.16.0.0/12
-A DOCKER-USER -j RETURN -s 10.0.0.0/8
-A DOCKER-USER -j RETURN -s 192.168.0.0/16

-A DOCKER-USER -p udp -m udp --sport 53 --dport 1024:65535 -j RETURN

-A DOCKER-USER -j ufw-user-forward

-A DOCKER-USER -j DROP -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -d 192.168.0.0/16
-A DOCKER-USER -j DROP -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -d 0.0.0.0/8
-A DOCKER-USER -j DROP -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -d 172.16.0.0/12
-A DOCKER-USER -j DROP -p udp -m udp --dport 0:32767 -d 192.168.0.0/16
-A DOCKER-USER -j DROP -p udp -m udp --dport 0:32767 -d 0.0.0.0/8
-A DOCKER-USER -j DROP -p udp -m udp --dport 0:32767 -d 172.16.0.0/12

-A DOCKER-USER -j RETURN
COMMIT
# END UFW AND DOCKER
```

If your VM is on a private network, your need to change 3 of the that range (`/12`, `/8` or `/16`)
