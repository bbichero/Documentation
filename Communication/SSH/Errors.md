SSH Errors
------

When trying to connect to remote host with public key:
```
Permission denied (publickey,gssapi-keyex,gssapi-with-mic)
```

Make sur the remove directory has correct rights:
```
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
```
