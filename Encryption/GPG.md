GPG
------
From tutorial [here](https://ekaia.org/blog/2009/05/10/creating-new-gpgkey/)
and [here](https://varrette.gforge.uni.lu/blog/2017/03/14/tutorial-gpg-gnu-privacy-guard/#linuxbsd)

Requirements:
```
Distribution: MacOS / Linux like
gpg (GnuPG) 2.2.13
```

Get preconfigurer `gpg.conf` who implement [GPG best practice](https://riseup.net/en/security/message-security/openpgp/best-practices):
```
cd ~/.gnupg
wget https://raw.githubusercontent.com/ioerror/duraconf/master/configs/gnupg/gpg.conf
```

Generate key:
```
gpg2 --full-gen-key --expert
```

Choose key kind and key specification (valid time, name, email, comment, ..)
A valid time of 2years is recommended

Set primary uid to key:
```
gpg --edit-key ${EMAIL_KEY}
gpg> uid 1
gpg> primary
```

Add image to your key (jpeg only):
```
gpg --edit-key ${EMAIL_KEY}
gpg> addphoto
Enter JPEG filename for photo ID: ${ABSOLUTE_PATH_TO_IMAGE}
gpg> save
```

Edit hash preferences of key:
```
gpg --edit-key ${EMAIL_KEY}
gpg> setpref SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZI
gpg> save
```

Add a new signing subkey to your key:
```
gpg --expert --edit-key ${EMAIL_KEY}
gpg> addkey
```
