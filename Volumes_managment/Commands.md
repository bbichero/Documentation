Commands
------

Remove disk label from disk:
```
sudo dd if=/dev/zero of=/dev/${DISK_NAME} count=16
```
(Nicer method must exit)
