OSDs
------

Zap device disk:
```
ceph-volume lvm zap ${LVM_GROUP}/${LVM_VOLUME}
```

Create OSDs:
```
ceph-volume lvm create --data ${LVM_GROUP}/${LVM_VOLUME}
```
