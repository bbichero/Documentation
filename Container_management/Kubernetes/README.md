Kubernetes
------

Requirements:
```
VMs or baremetal server with:
- Docker 18
- Debian Buster(10)
```

### Docs:
- [Installation](./Installation.md)
- [Usefull commands](./UsefullCommands.md)
- [Errors](./Errors.md)

### Example of kubernetes yaml configuration, to apply
- [Simple pod with shell](./examples/jumpod.yaml)
- [Simple namespace](./examples/other-ns.yaml)
- [Simple replication controller](./examples/other-rc.yaml)
- [Simple service](./examples/other-svc.yaml)
- [Simple pod with service exposing port](./examples/app.yaml)
- [Simple pod with health checks](./examples/health.yaml)
- [Simple pod with custom env variable](./examples/env.yaml)
- [Simple namespace](./examples/ns.yaml)
- [Simple pod with shared volume](./examples/volume.yaml)
